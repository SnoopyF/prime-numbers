package main

import (
	"fmt"
	"sync/atomic"
)

func main() {
	var numVector []uint64
	num := uint64(2)

	for {
		numVector = append(numVector, num)
		atomic.AddUint64(&num, 1)

		go printer(chkNum(numVector, num))
	}
}

func printer(num <-chan uint64) {
	for n := range num {
		if n != 0 {
			fmt.Println(n)
		}
	}
}

func chkNum(nums []uint64, num uint64) <-chan uint64 {
	out := make(chan uint64)
	go func() {
		out <- func() uint64 {
			for _, n := range nums {
				if num%n == 0 {
					return 0
				}
			}
			return num
		}()
		close(out)
	}()
	return out
}
